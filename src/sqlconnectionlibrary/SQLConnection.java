package sqlconnectionlibrary;

import common.ConfReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ronal
 */
public class SQLConnection {
    
    private static final String DRIVER = "jdbc:postgresql://";
    
    private Connection connection;
    private String user;
    private String password;
    private String driver;
    private ConfReader confReader;
    
    public SQLConnection() {
        confReader = new ConfReader();
        driver = DRIVER + confReader.get("host")+ ":" +confReader.get("port") + "/" + confReader.get("name");
        user = confReader.get("user");
        password = confReader.get("password");
    }
    
    public Connection connect() {
        try {
            connection = DriverManager.getConnection(driver,user,password);
        } catch(SQLException ex) {
            System.err.println("Class SQLConnection.connect dice: \n"
                    + "Ocurrio un error al momento de conectar el driver para realizar la conexion a la base de datos.");
        }
        return connection;           
    }
    public void closeConnection() {
        try {
            connection.close();
        }catch(SQLException ex) {
            System.err.println("Class SQLConnection.closeConnection dice: \n"
                    + "Ocurrio un error al momento de cerrar la conexion a la base de datos.");
        }
            
    }
   
}
