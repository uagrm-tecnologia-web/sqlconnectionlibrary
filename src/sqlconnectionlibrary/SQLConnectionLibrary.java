package sqlconnectionlibrary;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ronal
 */
public class SQLConnectionLibrary {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            SQLConnection connection = new SQLConnection();
            String query = "SELECT * FROM public.users WHERE email=?";
            PreparedStatement ps = connection.connect().prepareStatement(query);
            ps.setString(1, "ronaldorivero@uagrm.edu.bo");
            ResultSet rs = ps.executeQuery();
            System.out.println(rs.next());
        }catch(SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
}
